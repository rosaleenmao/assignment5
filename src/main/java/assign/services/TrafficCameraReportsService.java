package assign.services;

import assign.domain.Output;
import assign.domain.Report;
import assign.domain.Reports;

import java.util.List;


public interface TrafficCameraReportsService {
	public void loadFromSources() throws Exception;
	
	public Output getReportByCameraId(String cameraId) throws Exception;
    
    public String getAllReportsByCameraMfg(String cameraMfg) throws Exception; 
    
    public String getAllReportsByZipcode(String zipcode) throws Exception;
}
