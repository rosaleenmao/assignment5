package assign.domain;

import java.util.List;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

@XmlRootElement(name = "output")
@XmlAccessorType
@Entity
@Table(name = "traffic_camera_reports")
public class Output {

	private String locationName;
	private String cameraStatus;
	private String cameraMfg;
	private String zipcode;
	private String cameraId;
	private String count;

	public Output() {
	}
	
	//getters

    @XmlElement(name = "location_name")
	@Column(name = "location_name")
	public String getLocationName() {
		return locationName;
	}

    @XmlElement(name = "camera_status")
	@Column(name = "camera_status")
	public String getCameraStatus() {
		return cameraStatus;
	}

    @XmlElement(name = "camera_mfg")
	@Column(name = "camera_mfg")
	public String getCameraMfg() {
		return cameraMfg;
	}

    @XmlElement(name = "zipcode")
	@Column(name = "zipcode")
	public String getZipcode() {
		return zipcode;
	}

    @XmlElement(name = "camera_id")
	@Id
	@Column(name = "camera_id")
	public String getCameraId() {
		return cameraId;
	}

    @XmlElement(name = "count")
	public String getCount() {
		return count;
	}
	
	//setters
	
	public void setLocationName(String location_name) {
		this.locationName = location_name;
	}
	
	public void setCameraStatus(String camera_status) {
		this.cameraStatus = camera_status;
	}
	
	public void setCameraMfg(String camera_mfg) {
		this.cameraMfg = camera_mfg;
	}
	
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	public void setCameraId(String camera_id) {
		this.cameraId = camera_id;
	}
	
	public void setCount(String count) {
		this.count = count;
	}
}