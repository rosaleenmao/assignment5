package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "response")
@XmlAccessorType
public class Reports {

    private List<Report> reports = null;
 

    @XmlElement(name = "row")
    public List<Report> getReports() {
        return reports;
    }
 
    public void setReports(List<Report> reports) {
        this.reports = reports;
    }	
}
