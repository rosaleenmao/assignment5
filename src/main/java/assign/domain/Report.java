package assign.domain;

import java.util.List;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

@XmlRootElement(name = "row")
@XmlAccessorType
@Entity
@Table(name = "traffic_camera_reports")
public class Report {

	private String location_name;
	private String camera_status;
	private String camera_mfg;
	private String zipcode;
	private String camera_id;
	private int count;

	public Report() {
	}
	
	//getters

    @XmlElement(name = "location_name")
	@Column(name = "location_name")
	public String getLocationName() {
		return location_name;
	}

    @XmlElement(name = "camera_status")
	@Column(name = "camera_status")
	public String getCameraStatus() {
		return camera_status;
	}

    @XmlElement(name = "camera_mfg")
	@Column(name = "camera_mfg")
	public String getCameraMfg() {
		return camera_mfg;
	}

    @XmlElement(name = "zipcode")
	@Column(name = "zipcode")
	public String getZipcode() {
		return zipcode;
	}

    @XmlElement(name = "camera_id")
	@Id
	@Column(name = "camera_id")
	public String getCameraId() {
		return camera_id;
	}
    
	@Column(name = "count")
	public int getCount() {
		return count;
	}
	
	//setters
	
	public void setLocationName(String location_name) {
		this.location_name = location_name;
	}
	
	public void setCameraStatus(String camera_status) {
		this.camera_status = camera_status;
	}
	
	public void setCameraMfg(String camera_mfg) {
		this.camera_mfg = camera_mfg;
	}
	
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	public void setCameraId(String camera_id) {
		this.camera_id = camera_id;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
}