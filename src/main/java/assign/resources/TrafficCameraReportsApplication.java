package assign.resources;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/trafficcamerareports")
public class TrafficCameraReportsApplication extends Application {
	
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> classes = new HashSet<Class<?>>();
	
	public TrafficCameraReportsApplication() {  
	}
	
	@Override
	public Set<Class<?>> getClasses() { 
		return classes;
	}
	
	@Override
	public Set<Object> getSingletons() {
		try {
			singletons.add(new TrafficCameraReportsResource());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return singletons;
	}
	
	
}
