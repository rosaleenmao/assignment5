package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.domain.Output;
import assign.domain.Report;
import assign.domain.Reports;
import assign.services.TrafficCameraReportsService;
import assign.services.TrafficCameraReportsServiceImpl;

@Path("/")
public class TrafficCameraReportsResource {

	TrafficCameraReportsService trafficCameraReportsService;
	
	public TrafficCameraReportsResource() throws Exception {
		this.trafficCameraReportsService = new TrafficCameraReportsServiceImpl();	
		trafficCameraReportsService.loadFromSources();
	}

	@GET
	@Path("/")
	@Produces("text/html")
	public String helloWorld() {		
		return "Welcome to Traffic Camera Reports REST API for ETL.";		
	}

	@GET
	@Path("/reports/{camera_id}")
	@Produces("application/xml")
	public Response getReport(@PathParam("camera_id") String cameraId) throws Exception {

		Output output = this.trafficCameraReportsService.getReportByCameraId(cameraId);
		
		if (output == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		output.setCount(null);
		
		return Response.ok(output, MediaType.APPLICATION_XML).build();
	}
	
	@GET
	@Path("/reports")
	@Produces("application/xml")
	public Response getAllReports(@QueryParam("camera_mfg") String cameraMfg, @QueryParam("zipcode") String zipcode) throws Exception {

		final Output finalOutput = new Output();
		if (cameraMfg != null && zipcode == null) {
			String count = this.trafficCameraReportsService.getAllReportsByCameraMfg(cameraMfg);
			finalOutput.setCameraMfg(cameraMfg);
			if (count != null) {
				finalOutput.setCount(count);
			} else {
				finalOutput.setCount(null);
			}
		} else if (zipcode != null && cameraMfg == null) {
			String count = this.trafficCameraReportsService.getAllReportsByZipcode(zipcode);
			finalOutput.setZipcode(zipcode);
			if (count != null) {
				finalOutput.setCount(count);
			} else {
				finalOutput.setCount(null);
			}
		} else {
			return Response.status(Status.BAD_REQUEST).build();
		}
			    
		return Response.ok(finalOutput, MediaType.APPLICATION_XML).build();    
	}
}